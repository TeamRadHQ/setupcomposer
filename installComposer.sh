#!/bin/bash
#Create bin dierctory, link PHP and export to PATH
mkdir -p ~/bin;
ln -s /usr/local/bin/php-5.6 ~/bin/php;
echo "export PATH=~/bin/:\$PATH" >> ~/.bash_profile;
echo "export PATH=~/bin/:\$PATH" >> ~/.bashrc;
mkdir -p ~/.php/5.6;
echo "extension = phar.so" >> ~/.php/5.6/phprc;
# Download & Install Composer
curl -s https://getcomposer.org/installer | php -- --install-dir=~/bin;
chmod u+x ~/bin/composer.phar
mv ~/bin/composer.phar ~/bin/composer
